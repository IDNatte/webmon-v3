document.addEventListener("DOMContentLoaded", () => {
  let stream = new EventSource("/stream");
  let { signal, component } = reef;

  let timestampSignal = signal();
  let hostnameSignal = signal();
  let cpuStreamLabelSignal = signal();
  let networkSignal = signal();
  let runAppSignal = signal();
  let appnetSignal = signal();
  let jmsgSignal = signal();
  let diskInfoSignal = signal();
  let memInfoSignal = signal();
  let hwtempSignal = signal();

  let smoothieSeriesCPU = new TimeSeries();
  let smoothieSeriesMem = new TimeSeries();

  function hostname() {
    if (hostnameSignal.value) {
      return `${hostnameSignal.value}`;
    } else {
      return `N/A`;
    }
  }

  function timestamp() {
    if (timestampSignal.value) {
      return `${new Date(timestampSignal.value).toLocaleString("en-US")}`;
    } else {
      return `N/A`;
    }
  }

  function network() {
    if (!networkSignal.value) {
      return `
      <tr>
        <td>N/A</td>
        <td>N/A</td>

        <td>
          <span class="badge bg-danger">N/A</span>
        </td>
        <td>
          <span class="badge bg-success">N/A</span>
        </td>
      </tr>
      `;
    }

    if (networkSignal.value.length >= 0) {
      return `${networkSignal.value
        .map(
          (item) =>
            `<tr>
              <td>${item.interface}</td>
              <td>${item.ip_addr[0] ? item.ip_addr[0] : "N/A"}</td>
              <td>
                <span class="badge bg-danger">${filesize(item.transfer)}</span>
              </td>
              <td>
                <span class="badge bg-success">${filesize(item.receive)}</span>
              </td>
            </tr>`
        )
        .join("")}`;
    }
  }

  function runningApp() {
    if (!runAppSignal.value) {
      return `
      <tr>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
      </tr>
      `;
    }

    // return `${JSON.stringify(runAppSignal.value)}`;

    if (runAppSignal.value.length >= 0) {
      return `${runAppSignal.value
        .map(
          (item) =>
            `<tr>
              <td>${item.proc_info.pid}</td>
              <td>${item.proc_name}</td>
              <td>${Math.round(item.used_resource.cpu * 100) / 100} %</td>
              <td>${Math.round(item.used_resource.mem * 100) / 100} %</td>
            </tr>`
        )
        .join("")}`;
    }
  }

  function appNetInfo() {
    if (!appnetSignal.value) {
      return `
      <tr>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
      </tr>
      `;
    }

    if (appnetSignal.value.length >= 0) {
      return `${appnetSignal.value
        .map(
          (item) =>
            `<tr>
            <th scope="row"><a href="#">${item.pid}</a></th>
            <td>${item.proc_name}</td>
            <td>${item.address.local_address}</td>
            <td>${item.address.local_port}</td>
            <td>${item.address.remote_address}</td>
            <td>${item.address.remote_port}</td>
            </tr>`
        )
        .join("")}`;
    }
  }

  function jmsgInfo() {
    if (!jmsgSignal.value) {
      return `
      <tr>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
      </tr>
      `;
    }

    if (jmsgSignal.value.length >= 0) {
      return `${jmsgSignal.value
        .map(
          (item) =>
            `<tr>
              <td>${item.SYSLOG_TIMESTAMP ? item.SYSLOG_TIMESTAMP : "N/A"}</td>
              <td>${item._CMDLINE ? item._CMDLINE : "N/A"}</td>
              <td>${item.MESSAGE ? item.MESSAGE : "N/A"}</td>
              <td>${item._EXE ? item._EXE : "N/A"}</td>
            </tr>`
        )
        .join("")}`;
    }
  }

  function cpuStream() {
    if (cpuStreamLabelSignal.value) {
      return `CPU Percentage (${cpuStreamLabelSignal.value} core)`;
    }

    if (!cpuStreamLabelSignal.value) {
      return `CPU Percentage`;
    }
  }

  function diskInfo() {
    if (!diskInfoSignal.value) {
      return `
      <tr>
        <td>N/A</td>
        <td>
          <span class="badge bg-danger">N/A</span>
        
        </td>
        <td>
          <span class="badge bg-success">N/A</span>
        </td>
        <td>
          <span class="badge bg-warning">N/A</span>
        </td>
      </tr>
      `;
    }

    if (diskInfoSignal.value.length >= 0) {
      return `${diskInfoSignal.value
        .map(
          (item) =>
            `<tr>
              <td>${item.mount_point}</td>
              <td>
                <span class="badge bg-danger">${filesize(item.used)}</span>
              
              </td>
              <td>
                <span class="badge bg-success">${filesize(item.free)}</span>
              </td>
              <td>
                <span class="badge bg-warning">${filesize(item.total)}</span>
              </td>
            </tr>`
        )
        .join("")}`;
    }
  }

  function memInfo() {
    if (!memInfoSignal.value) {
      return `
      <tr>
        <td>
          <span class="badge bg-danger">N/A</span>
        
        </td>
        <td>
          <span class="badge bg-success">N/A</span>
        </td>
        <td>
          <span class="badge bg-warning">N/A</span>
        </td>
      </tr>
      `;
    }

    if (memInfoSignal.value) {
      return `
      <tr>
        <td>
          <span class="badge bg-danger">${filesize(
            memInfoSignal.value.used_memory
          )}</span>
        
        </td>
        <td>
          <span class="badge bg-success">${filesize(
            memInfoSignal.value.free_memory
          )}</span>
        </td>
        <td>
          <span class="badge bg-warning">${filesize(
            memInfoSignal.value.total_memory
          )}</span>
        </td>
      </tr>`;
    }
  }

  function tempInfo() {
    if (!hwtempSignal.value) {
      return `
      <tr>
        <td>N/A</td>
        <td>
          <span class="badge bg-danger">N/A</span>
        
        </td>
      </tr>
      `;
    }

    if (hwtempSignal.value.length >= 0) {
      return `${hwtempSignal.value
        .map(
          (item) =>
            `<tr>
              <td>${item.label}</td>
              <td>
                <span class="badge bg-danger">${item.temperature} &#176; Celcius</span>
              </td>
            </tr>`
        )
        .join("")}`;
    }
  }

  function getRandomBrightColor() {
    let color;
    do {
      const r = Math.floor(Math.random() * 155) + 100; // Restricting the range to avoid dark colors and yellowish tones
      const g = Math.floor(Math.random() * 155) + 100; // Restricting the range to avoid dark colors and yellowish tones
      const b = Math.floor(Math.random() * 155) + 100; // Restricting the range to avoid dark colors and yellowish tones
      color = `#${((r << 16) | (g << 8) | b).toString(16).padStart(6, "0")}`;
    } while (
      isDarkColor(color) ||
      isBlackColor(color) ||
      isYellowishColor(color)
    ); // Keep generating until we get a valid color
    return color;
  }

  // Function to check if a color is dark
  function isDarkColor(color) {
    const rgb = parseInt(color.substring(1), 16);
    const r = (rgb >> 16) & 0xff;
    const g = (rgb >> 8) & 0xff;
    const b = (rgb >> 0) & 0xff;
    return Math.sqrt(r * r * 0.241 + g * g * 0.691 + b * b * 0.068) < 130; // Adjust threshold as needed
  }

  // Function to check if a color is black
  function isBlackColor(color) {
    return color === "#000000";
  }

  // Function to check if a color is yellowish
  function isYellowishColor(color) {
    const rgb = parseInt(color.substring(1), 16);
    const r = (rgb >> 16) & 0xff;
    const g = (rgb >> 8) & 0xff;
    const b = (rgb >> 0) & 0xff;
    return r > 200 && g > 200 && b < 100; // Adjust threshold as needed
  }

  const smoothieCPU = new SmoothieChart({
    responsive: true,
    tooltip: true,
    maxValue: 100,
    minValue: 0,
    grid: { fillStyle: "#ffffff" },
    labels: { fillStyle: "#000000" },
  });

  const smoothieMemory = new SmoothieChart({
    responsive: true,
    tooltip: true,
    maxValue: 100,
    minValue: 0,
    grid: { fillStyle: "#ffffff" },
    labels: { fillStyle: "#000000" },
  });

  stream.addEventListener("message", (event) => {
    timestampSignal.value = JSON.parse(event.data).data.probe_time;
    networkSignal.value = JSON.parse(event.data).data.system_info.net_info;
    hostnameSignal.value = JSON.parse(event.data).data.system_info.hostname;
    runAppSignal.value = JSON.parse(event.data).data.application.running_app;
    appnetSignal.value = JSON.parse(event.data).data.application.app_net_usage;
    jmsgSignal.value = JSON.parse(event.data).data.journal_info;
    diskInfoSignal.value = JSON.parse(event.data).data.system_info.disk;
    memInfoSignal.value = JSON.parse(event.data).data.system_info.memory;
    hwtempSignal.value = JSON.parse(event.data).data.system_info.hw_temp;

    cpuStreamLabelSignal.value = JSON.parse(
      event.data
    ).data.system_info.cpu.core;

    smoothieSeriesMem.append(
      Date.now(),
      JSON.parse(event.data).data.system_info.memory.percentage
    );

    smoothieSeriesCPU.append(
      Date.now(),
      JSON.parse(event.data).data.system_info.cpu.load
    );
  });

  smoothieCPU.addTimeSeries(smoothieSeriesCPU, {
    lineWidth: 2,
    strokeStyle: getRandomBrightColor(),
  });

  smoothieMemory.addTimeSeries(smoothieSeriesMem, {
    lineWidth: 2,
    strokeStyle: getRandomBrightColor(),
  });

  smoothieCPU.streamTo(document.querySelector(".smoothie-cpu"), 3000);
  smoothieMemory.streamTo(document.querySelector(".smoothie-mem"), 3000);

  component(".rc-timestamp", timestamp);
  component(".rc-network", network);
  component(".rc-hostname", hostname);
  component(".rc-runapp", runningApp);
  component(".rc-appnet", appNetInfo);
  component(".rc-jmsg", jmsgInfo);
  component(".rc-cpustream", cpuStream);
  component(".rc-disk", diskInfo);
  component(".rc-mem", memInfo);
  component(".rc-hwtemp", tempInfo);
});

document.addEventListener("reef:signal", () => {});
