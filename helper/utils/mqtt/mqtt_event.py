import time
from helper.constant.constant import MSGQUEUE


def mqtt_to_stream():
    while True:
        msg_queue = MSGQUEUE.get()
        if msg_queue is None:
            time.sleep(1)
            continue

        yield f"data: {msg_queue}\n\n"
