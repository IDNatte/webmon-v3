from helper.constant.constant import MSGQUEUE
import msgpack
import json


def mqtt_controller_handler(client, userdata, message):
    data = msgpack.unpackb(message.payload)
    MSGQUEUE.put(json.dumps({"data": data}))
