from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from queue import Queue

from helper.module.webmon_mqtt import WebmonMQTT

MQTTINSTANCE = WebmonMQTT()
MIGRATOR = Migrate()
MSGQUEUE = Queue()
DB = SQLAlchemy()
