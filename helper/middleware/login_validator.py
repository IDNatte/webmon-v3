from flask import redirect
from flask import session
from flask import url_for
from flask import flash

from functools import wraps

from model.user import User


def login_required(function):
    @wraps(function)
    def verify_login(*args, **kwargs):
        if "wid" in session:
            user_data = User.query.get(session.get("wid"))
            return function(
                {
                    "id": user_data.id,
                    "username": user_data.username,
                    "topic": user_data.user_topic,
                },
                *args,
                **kwargs
            )

        else:
            flash("Please login first", "error")
            return redirect(url_for("main.login"))

    return verify_login
