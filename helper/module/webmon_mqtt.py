import paho.mqtt.client as mqtt

import secrets


class WebmonMQTT:
    def __init__(self, app=None):
        self.app = app
        self.mqtt = mqtt.Client()

        # app config
        self.mqtt_username = None
        self.mqtt_password = None
        self.mqtt_host = None
        self.mqtt_port = 1883
        self.mqtt_client_id = None
        self.mqtt_QOS = None
        self.mqtt_topics = []

        # connect, disconnect, also on message callback
        self.on_connect = None
        self.on_disconnect = None
        self.on_message = None

        self.mqtt = None

        if app is not None:
            self.init_app(app)

    def init_app(self, app=None):
        self.mqtt_host = app.config.get("MQTT_HOST", "127.0.0.1")
        self.mqtt_port = app.config.get("MQTT_PORT", 1883)
        self.mqtt_username = app.config.get("MQTT_USERNAME", "admin")
        self.mqtt_password = app.config.get("MQTT_PASSWORD", "admin")
        self.mqtt_client_id = (
            f'webmond-{app.config.get("MQTT_CLIENT_ID", secrets.token_hex())}'
        )
        self.mqtt_QOS = app.config.get("MQTT_QOS", 0)

        # initialize mqtt client
        self.mqtt = mqtt.Client(client_id=self.mqtt_client_id)

        self._connect()

    def _connect(self):
        self.mqtt.username_pw_set(self.mqtt_username, self.mqtt_password)
        self.mqtt.connect(host=self.mqtt_host, port=self.mqtt_port)

        self.mqtt.loop_start()

    def subscribe(self, topic):
        self.mqtt.subscribe(topic, qos=self.mqtt_QOS)

    def on_topic(self, topic, callback):
        self.__append_topic(topic)
        self.mqtt.message_callback_add(topic, callback)

    def clean_handle(self):
        for idx, topic in enumerate(self.mqtt_topics):
            self.mqtt_topics.pop(idx)
            self.mqtt.message_callback_remove(topic)

    def __append_topic(self, topic):
        if topic not in self.mqtt_topics:
            self.mqtt_topics.append(topic)
