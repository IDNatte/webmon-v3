from flask import Response, render_template
from flask import Blueprint
from flask import redirect
from flask import request
from flask import session
from flask import url_for

from helper.middleware.login_validator import login_required

from helper.utils.mqtt.mqtt_handler import mqtt_controller_handler
from helper.utils.mqtt.mqtt_event import mqtt_to_stream
from helper.constant.constant import MQTTINSTANCE, MSGQUEUE

from helper.security.password_module import check_passwd

from model.user import User


main = Blueprint("main", __name__)


@main.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")

        account_checker = User.query.filter_by(username=username).first()

        if account_checker and check_passwd(account_checker.password, password) == True:
            session["wid"] = str(account_checker.id)
            return redirect(url_for("main.index"))

        else:
            error = "Invalid username or password"
            return render_template("template/login/index.html", error=error)
    else:
        return render_template("template/login/index.html")


@main.route("/")
@login_required
def index(account):
    users = User.query.all()
    return render_template("template/admin/index.html", account=account, user=users)


@main.route("/logout")
@login_required
def logout(_):
    session.pop("wid", None)
    return redirect(url_for("main.login"))


@main.route("/stream")
@login_required
def stream(account):
    return Response(
        mqtt_to_stream(), mimetype="text/event-stream", content_type="text/event-stream"
    )


@main.route("/monitor/<device>")
@login_required
def monitor_device(account, device):
    MQTTINSTANCE.mqtt_topics
    if len(MQTTINSTANCE.mqtt_topics) > 0:
        MQTTINSTANCE.clean_handle()

    users = User.query.all()
    device_data = User.query.filter_by(id=device).first()
    MQTTINSTANCE.subscribe(device_data.user_topic)
    # print(MSGQUEUE.get())
    MQTTINSTANCE.on_topic(device_data.user_topic, mqtt_controller_handler)
    return render_template(
        "template/monitor/index.html",
        account=account,
        device=device_data.device,
        user=users,
    )
