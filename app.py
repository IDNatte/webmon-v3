from flask import Flask

# 3rd party library
from getpass import getpass

import click
import toml

# web module
from controller import web_error
from controller import main

# database stuff module
from helper.security.password_module import create_password
from helper.constant.constant import MQTTINSTANCE
from helper.constant.constant import MIGRATOR
from helper.constant.constant import DB

from model.user import User


def init_app(test_config=None):

    app = Flask(__name__, instance_relative_config=False)
    app.config.from_file("./config/config.toml", load=toml.load)

    # database initializer
    DB.init_app(app)
    MIGRATOR.init_app(app, db=DB)

    MQTTINSTANCE.init_app(app=app)

    # seeder
    @app.cli.command("create-user")
    @click.argument("name")
    def create_user(name):
        password = getpass("Enter password : ")
        confirm_pass = getpass("Re-Enter your password : ")
        device = input("Device name (E.g. hostname) : ").strip() or None

        if confirm_pass == password and device:
            is_available = User.query.filter_by(username=name).first()

            if not is_available:
                add_user = User(
                    username=name,
                    password=create_password(password),
                    user_topic=f"webmon/telemetry/{device}",
                    device=device,
                )
                DB.session.add(add_user)
                DB.session.commit()
                print(f"[*] User {name} created !")

            else:
                print("[!] User already registered")

        else:
            print("\n[!] please make sure password match and insert device name...\n")

    # web router
    app.register_blueprint(web_error.web_error)
    app.register_blueprint(main.main)

    return app
