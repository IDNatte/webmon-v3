from helper.constant.constant import DB

from helper.database.database_helper import random_id_generator


class User(DB.Model):
    __tablename__ = "webmon_user"

    id = DB.Column(
        DB.String(200), primary_key=True, nullable=False, default=random_id_generator
    )
    username = DB.Column(DB.String(50), nullable=False)
    password = DB.Column(DB.String(200), nullable=False, unique=True)
    user_topic = DB.Column(DB.String(200), nullable=False)
    device = DB.Column(DB.String(150), nullable=False)
